<?php

include("Emplacement.php");
include("Vendeur.php");
include("Categorie.php");
include("Annonce.php");

class Connexion{
    private $dbh;

    function __construct($bd){
        try {
            $this->dbh = new PDO('mysql:host=localhost;dbname='.$bd.';charset=utf8',"root");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function getAllAnnonces(){
        $stmt = $this->dbh->prepare('SELECT *, annonce.id as annonceID, annonce.nom_Annonce as nomAnnonce, annonce.description as descriptionAnnonce FROM annonce INNER JOIN vendeur ON annonce.le_vendeur_id = vendeur.id JOIN emplacement ON annonce.un_emplacement_id = emplacement.id JOIN categorie ON annonce.la_categorie_id = categorie.id');
        $stmt->execute();
        $annoncePOO = array();
        while ($row = $stmt->fetch()) {
            $emplacement = new Emplacement($row['adresse'], $row['code_postal']);
            $categorie = new Categorie($row['libelle']);
            $vendeur = new Vendeur($row['le_vendeur_id'], $row['nom'], $row['num_tel'], $row['mail']);
            $annonce = new Annonce($row['annonceID'],$row['nomAnnonce'],$row['descriptionAnnonce'],$row['quantite'],$row["image"],$vendeur,$categorie,$emplacement);
            array_push($annoncePOO, $annonce);
        }
        return $annoncePOO;
    }

    function getAllCategories(){
        $stmt2 = $this->dbh->prepare('SELECT * FROM categorie');
        $stmt2->execute();
        $categoriesPOO = array();
        while ($row = $stmt2->fetch()) {
            $categorie = new Categorie($row['libelle']);
            array_push($categoriesPOO, $categorie);
        }
        return $categoriesPOO;
    }
}   