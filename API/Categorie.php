<?php

class Categorie implements JsonSerializable{
    private $libellé;

    function __construct($libellé){
        $this->libellé = $libellé;
    }

    public function getLibellé(){
        return $this->libellé;
    }

    public function jsonSerialize (){
        return [
            "libelle" => $this->libellé
        ];
    }
}