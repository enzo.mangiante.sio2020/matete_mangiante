-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 08 oct. 2021 à 13:13
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `matete`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE `annonce` (
  `id` int(11) NOT NULL,
  `nomAnnonce` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `quantite` varchar(1000) NOT NULL,
  `id_Vendeur` int(11) NOT NULL,
  `id_Categorie` int(11) NOT NULL,
  `id_Emplacement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`id`, `nomAnnonce`, `description`, `quantite`, `id_Vendeur`, `id_Categorie`, `id_Emplacement`) VALUES
(1, 'Mes patates', 'Je vend des patates sur perpi', '150', 1, 1, 2),
(2, 'Mes pommes', 'Je vend aussi des pommes sur canet', '50', 1, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `libelle` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'Legume'),
(2, 'Fruit');

-- --------------------------------------------------------

--
-- Structure de la table `emplacement`
--

CREATE TABLE `emplacement` (
  `id` int(11) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `nomVille` varchar(100) NOT NULL,
  `codePostal` varchar(50) NOT NULL,
  `coX` varchar(100) NOT NULL,
  `coY` varchar(100) NOT NULL,
  `id_Vendeur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `emplacement`
--

INSERT INTO `emplacement` (`id`, `adresse`, `nomVille`, `codePostal`, `coX`, `coY`, `id_Vendeur`) VALUES
(2, '24 rue des Carlettes', 'Perpignan', '66000', '', '', 1),
(3, '40 rue vallée du Rhone', 'Canet en Roussillon', '66140', '', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `vendeur`
--

CREATE TABLE `vendeur` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `numTel` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vendeur`
--

INSERT INTO `vendeur` (`id`, `nom`, `numTel`, `mail`, `password`) VALUES
(1, 'Mathieu', '0606060606', 'mathieu@mail.com', 'motdepasse');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Annonce_Vendeur_FK` (`id_Vendeur`),
  ADD KEY `Annonce_Categorie0_FK` (`id_Categorie`),
  ADD KEY `Annonce_Emplacement1_FK` (`id_Emplacement`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `emplacement`
--
ALTER TABLE `emplacement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Emplacement_Vendeur_FK` (`id_Vendeur`);

--
-- Index pour la table `vendeur`
--
ALTER TABLE `vendeur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `annonce`
--
ALTER TABLE `annonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `emplacement`
--
ALTER TABLE `emplacement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `vendeur`
--
ALTER TABLE `vendeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `Annonce_Categorie0_FK` FOREIGN KEY (`id_Categorie`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `Annonce_Emplacement1_FK` FOREIGN KEY (`id_Emplacement`) REFERENCES `emplacement` (`id`),
  ADD CONSTRAINT `Annonce_Vendeur_FK` FOREIGN KEY (`id_Vendeur`) REFERENCES `vendeur` (`id`);

--
-- Contraintes pour la table `emplacement`
--
ALTER TABLE `emplacement`
  ADD CONSTRAINT `Emplacement_Vendeur_FK` FOREIGN KEY (`id_Vendeur`) REFERENCES `vendeur` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
