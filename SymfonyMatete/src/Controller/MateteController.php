<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Entity\Categorie;
use App\Entity\Emplacement;
use App\Entity\Vendeur;
use App\Form\AnnonceType;
use App\Form\EmplacementType;
use App\Form\VendeurType;
use App\Repository\AnnonceRepository;
use App\Repository\CategorieRepository;
use App\Repository\EmplacementRepository;
use App\Repository\VendeurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class MateteController extends AbstractController
{
    private $security;
    private $user;

    public function __construct(Security $security)
    {
        $this->security = $security;
        $this->user = $security->getUser();
    }

    /**
     * @Route("/annonces", name="annonces")
     * @Route("/", name="index")
     */
    public function annonces(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonces = $repo->findAll();
        $session = new Session(new NativeSessionStorage(), new AttributeBag());
        $session->has('panier') ? $panier = $session->get('panier') : $panier = [];

        return $this->render('matete/annonces.html.twig', [
            'annonces' => $annonces,
            'panier' => $panier,
        ]);
    }

    /**
     * @Route("/annonce/{id}", name="show")
     */
    public function show(AnnonceRepository $annonceRepo, $id): Response
    {
        $inPanier = false;

        $session = new Session(new NativeSessionStorage(), new AttributeBag());
        $session->has('panier') ? $panier = $session->get('panier') : $panier = [];

        if (array_key_exists($id, $panier)) {
            $inPanier = true;
        }

        return $this->render('matete/show.html.twig', [
            'annonce' => $annonceRepo->find($id),
            'inPanier' => $inPanier,
        ]);
    }

    /**
     * @Route("/vendeur/{id}", name="vendeur")
     */
    public function vendeur(VendeurRepository $vendeurRepo, AnnonceRepository $annonceRepo, $id): Response
    {
        return $this->render('matete/vendeur.html.twig', [
            'vendeur' => $vendeurRepo->find($id),
            'annonces' => $annonceRepo->findBy(['leVendeur' => $id]),
        ]);
    }

    /**
     * @Route("/profil/", name="show_profil")
     */
    public function showProfil(EntityManagerInterface $entityManager, Request $request): Response
    {
        $user = new Vendeur();
        // recup user session;
        $user = $this->security->getUser();
        $annonces = $user->lesAnnonces;
        $emplacements = $user->lesEmplacements;

        // formulaire emplacement
        $emplacement = new Emplacement();
        $form = $this->createForm(EmplacementType::class, $emplacement);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) { // vérif si envoyé et valide
            // sauvegarde de l’article en bdd
            $emplacement->setLeVendeur($user);
            $entityManager->persist($emplacement);
            $entityManager->flush();
            dump($emplacement);
            // redirection vers la page de visualisation de l’article
            return $this->redirectToRoute('show_profil');
        }

        return $this->render('matete/show_profil.html.twig', [
            'vendeur' => $user,
            'annonces' => $annonces,
            'emplacements' => $emplacements,
            'form' => $form->createView(),
        ]);

        return $this->render('matete/show_profil.html.twig', [
            'vendeur' => $user,
        ]);
    }

    /**
     * @Route("/deleteAnnonce/{id}", name="annonce_delete")
     */
    public function delete(EntityManagerInterface $entityManager, Annonce $annonce, $id): Response
    {
        $user = new Vendeur();
        $user = $this->security->getUser();

        // protection de l'url
        if (!in_array('ROLE_ADMIN', $user->roles)) {
            // récupère les ID des annonces de l'utilisateur connecté
            $Id_annonces_User = [];
            if (0 == count($user->lesAnnonces)) {
                $this->addFlash(
                    'annonce',
                    'Vous ne pouvez pas supprimer cette annonce, elle ne vous appartient pas'
                );

                return $this->redirectToRoute('annonces');
            }

            for ($i = 0; $i < count($user->lesAnnonces); ++$i) {
                array_push($Id_annonces_User, $user->lesAnnonces[$i]->getId());
            }

            // compare l'id entré en paramètre avec les ID des annonces de l'utilisateur connecté
            // cela sert à éviter qu'un utilisateur malveillant puisse éditer (via l'url)
            // une annonce qui ne lui appartient pas
            for ($i = 0; $i < count($user->lesAnnonces); ++$i) {
                if (!in_array($id, $Id_annonces_User)) {
                    throw $this->createNotFoundException("Vous n'avez pas accès à l'annonce ayant pour ID : " . $id); // phpcs:ignore
                }
            }
        }
        if (!$annonce) {
            $this->addFlash(
                'alert',
                "Il n'a pas d'annonce avec cet id"
            );

            return $this->redirectToRoute('annonces');
        }

        $entityManager->remove($annonce);
        $entityManager->flush();

        return $this->redirectToRoute('annonces');
    }

    /**
     * @Route("/deleteEmplacement/{id}" , name="emplacement_delete")
     */
    public function delEmplacement(EntityManagerInterface $entityManager, EmplacementRepository $emplacement, AnnonceRepository $annoncesRepo, $id) // phpcs:ignore
    {
        $emplacement = $emplacement->find($id);

        if (!$emplacement) {
            throw $this->createNotFoundException("Il n'y a pas d'emplacement ayant l'id : " . $id);
        }

        $lesAnnonces = $annoncesRepo->findBy(['unEmplacement' => $emplacement->getId()]);
        for ($i = 0; $i < count($lesAnnonces); ++$i) {
            $entityManager->remove($lesAnnonces[$i]);
        }
        $entityManager->remove($emplacement);
        $entityManager->flush();

        return $this->redirectToRoute('show_profil');
    }

    /**
     * @Route("/deletePanier/{id}" , name="panier_delete")
     */
    public function deletePanier($id)
    {
        $session = new Session(new NativeSessionStorage(), new AttributeBag());
        $session->has('panier') ? $panier = $session->get('panier') : $panier = [];
        unset($panier[$id]);
        $session->set('panier', $panier);

        if (!$id) {
            throw $this->createNotFoundException("Il n'y a pas d'annonce ayant l'id : " . $id);
        }

        return $this->redirectToRoute('panier');
    }

    /**
     * @Route("/deleteProfil/{id}", name="profil_delete")
     */
    public function deleteProfil(EntityManagerInterface $entityManager): Response
    {
        $profil = $this->security->getUser();
        $lesEmplacements = $profil->lesEmplacements;
        $lesAnnonces = $profil->lesAnnonces;

        $session = $this->get('session');
        $session = new Session();
        $session->invalidate();

        for ($i = 0; $i < count($lesEmplacements); ++$i) {
            $entityManager->remove($lesEmplacements[$i]);
        }

        for ($i = 0; $i < count($lesAnnonces); ++$i) {
            $entityManager->remove($lesAnnonces[$i]);
        }

        $entityManager->flush();
        $entityManager->remove($profil);
        $entityManager->flush();

        return $this->redirectToRoute('app_logout');
    }

    /**
     * @Route("/add", name="add")
     */
    public function add(EntityManagerInterface $entityManager, Request $request, CategorieRepository $cat, EmplacementRepository $emplacementRep): Response // phpcs:ignore
    {
        $user = new Vendeur();
        $user = $this->security->getUser(); // null or UserInterface, if logged in
        $annonce = new Annonce();
        $emplacementVide = false;

        $form = $this->createFormBuilder($annonce)
        ->add('nomAnnonce', TextType::class)
        ->add('description', TextareaType::class)
        ->add('quantite', IntegerType::class)
        ->add('image', TextType::class)
        ->add('laCategorie', EntityType::class, ['class' => Categorie::class, 'choices' => $cat->findAll()])
        ->add('unEmplacement', EntityType::class, ['class' => Emplacement::class, 'choices' => $emplacementRep->findBy(['leVendeur' => $user->id])]) // phpcs:ignore
        ->getForm();

        $form->handleRequest($request);
        if (count($emplacementRep->findBy(['leVendeur' => $user->id])) < 1) {
            $emplacementVide = true;
        }
        if ($form->isSubmitted() && $form->isValid()) { // vérif si envoyé et valide
            // sauvegarde de l’article en bdd
            $time = new \DateTimeImmutable();
            $time->format('H:i:s \O\n d-m-Y ');
            $annonce->setCreatedAt($time);
            $annonce->setLeVendeur($user);
            $entityManager->persist($annonce);
            $entityManager->flush();
            dump($annonce);
            // redirection vers la page de visualisation de l’article
            return $this->redirectToRoute('show', ['id' => $annonce->getId()]);
        }

        return $this->render('matete/add.html.twig', [
            'form' => $form->createView(),
            'emplacementVide' => $emplacementVide,
        ]);
    }

    /**
     * @Route("/editAnnonce/{id}", name="annonce_edit")
     */
    public function edit(EntityManagerInterface $entityManager, Annonce $annonce, $id, Request $request): Response
    {
        $form = $this->createForm(AnnonceType::class, $annonce);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) { // vérif si envoyé et valide
            // sauvegarde de l’article en bdd
            $time = new \DateTimeImmutable();
            $time->format('H:i:s \O\n d-m-Y ');
            $annonce->setCreatedAt($time);
            $entityManager->persist($annonce);
            $entityManager->flush();
            // redirection vers la page de visualisation de l’article
            return $this->redirectToRoute('show', ['id' => $annonce->getId()]);
        }

        return $this->render('matete/editAnnonce.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editProfil/{id}", name="profil_edit")
     */
    public function editProfil(EntityManagerInterface $entityManager, Vendeur $vendeur, $id, Request $request): Response
    {
        if ($this->user->id == $id) {
            $form = $this->createForm(VendeurType::class, $vendeur);
            $form->handleRequest($request);
            $vendeur->confirm_password = $vendeur->getPassword();
            if ($form->isSubmitted()) { // vérif si envoyé et valide
                // sauvegarde de l’article en bdd
                $entityManager->persist($vendeur);
                $entityManager->flush();
                // redirection vers la page de visualisation de l’article
                return $this->redirectToRoute('show_profil', ['id' => $vendeur->getId()]);
            }

            return $this->render('matete/edit_profil.html.twig', [
                'form' => $form->createView(),
            ]);
        } else {
            return $this->redirectToRoute('annonces');
        }
    }

    /**
     * @Route("/ajouter_panier/{id}", name="ajouter_panier")
     */
    public function addPanier($id, Annonce $annonce): Response
    {
        $session = new Session(new NativeSessionStorage(), new AttributeBag());
        $session->has('panier') ? $panier = $session->get('panier') : $panier = [];
        array_key_exists($id, $panier) ?: $panier[$id] = $annonce;
        // set and get session attributes
        $session->set('panier', $panier);

        return $this->redirectToRoute('annonces');
    }

    /**
     * @Route("/panier", name="panier")
     */
    public function showPanier(): Response
    {
        $session = new Session(new NativeSessionStorage(), new AttributeBag());
        $session->has('panier') ? $panier = $session->get('panier') : $panier = [];

        // faire un findById avec le tableau des id annonces
        $idAnnonces = array_keys($panier);
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonces = $repo->findBy(['id' => $idAnnonces]);

        return $this->render('matete/panier.html.twig', [
            'annonces' => $annonces,
        ]);
    }

    /**
     * @Route("/map", name="map")
     */
    public function map(): Response
    {
        return $this->render('matete/map.html.twig');
    }

    /**
     * @Route("/pdf", name="pdf")
     */
    public function pdf(): Response
    {
        $session = new Session(new NativeSessionStorage(), new AttributeBag());
        $session->has('panier') ? $panier = $session->get('panier') : $panier = [];
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        $annonces = $repo->findBy(['id' => $panier]);
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('matete/pdf.html.twig', [
            'title' => 'Welcome to our PDF Test',
            'annonces' => $annonces,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream('mypdf.pdf', [
            'Attachment' => true,
        ]);

        return $this->render('matete/map.html.twig');
    }
}
