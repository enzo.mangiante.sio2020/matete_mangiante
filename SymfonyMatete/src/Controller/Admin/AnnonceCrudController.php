<?php

namespace App\Controller\Admin;

use App\Entity\Annonce;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AnnonceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Annonce::class;
    }

    public function configureFields(string $pageName): iterable
    {
        if ('new' == $pageName) {
            return [
                TextField::new('nomAnnonce'),
                TextField::new('Description'),
                AssociationField::new('laCategorie'),
                AssociationField::new('leVendeur'),
                AssociationField::new('unEmplacement'),
            ];
        } else {
            return [
                IdField::new('id'),
                TextField::new('nomAnnonce'),
                TextField::new('Description'),
                AssociationField::new('laCategorie'),
                AssociationField::new('leVendeur'),
                AssociationField::new('unEmplacement'),
            ];
        }
    }
}
