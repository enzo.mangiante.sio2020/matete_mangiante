<?php

namespace App\Controller\Admin;

use App\Entity\Annonce;
use App\Entity\Categorie;
use App\Entity\Emplacement;
use App\Entity\Vendeur;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // redirect to some CRUD controller
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(AnnonceCrudController::class)->generateUrl());

        return $this->redirect($routeBuilder->setController(VendeurCrudController::class)->generateUrl());

        return $this->redirect($routeBuilder->setController(EmplacementCrudController::class)->generateUrl());

        return $this->redirect($routeBuilder->setController(CategorieCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('SymfonyMatete');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Categories', 'fas fa-list', Categorie::class);
        yield MenuItem::linkToCrud('Vendeurs', 'fas fa-list', Vendeur::class);
        yield MenuItem::linkToCrud('Annonces', 'fas fa-list', Annonce::class);
        yield MenuItem::linkToCrud('Emplacements', 'fas fa-list', Emplacement::class);
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
