<?php

namespace App\Entity;

use App\Repository\AnnonceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AnnonceRepository::class)
 */
class Annonce
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(
     * message="Ne peut pas être vide"
     * )
     * @Assert\Length(
     *      min = 10,
     *      max = 300,
     *      minMessage = " Votre description doit comporter minimum 10 caractères.",
     *      maxMessage = " Votre description doit comporter maximum 300 caractères."
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     * message="Ne peut pas être vide"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 30,
     *      minMessage = " Votre description doit comporter minimum 2 caractères.",
     *      maxMessage = " Votre description doit comporter maximum 30 caractères."
     * )
     */
    private $nomAnnonce;

    /**
     * @ORM\Column(type="integer", length=255)
     * @Assert\NotBlank(
     * message="Ne peut pas être vide"
     * )
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="lesAnnonces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $laCategorie;

    /**
     * @ORM\ManyToOne(targetEntity=Vendeur::class, inversedBy="lesAnnonces")
     */
    private $leVendeur;

    /**
     * @ORM\ManyToOne(targetEntity=Emplacement::class, inversedBy="lesAnnonces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $unEmplacement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_At;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNomAnnonce(): ?string
    {
        return $this->nomAnnonce;
    }

    public function setNomAnnonce(string $nomAnnonce): self
    {
        $this->nomAnnonce = $nomAnnonce;

        return $this;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(string $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getLaCategorie(): ?Categorie
    {
        return $this->laCategorie;
    }

    public function setLaCategorie(?Categorie $laCategorie): self
    {
        $this->laCategorie = $laCategorie;

        return $this;
    }

    public function getLeVendeur(): ?Vendeur
    {
        return $this->leVendeur;
    }

    public function setLeVendeur(?Vendeur $leVendeur): self
    {
        $this->leVendeur = $leVendeur;

        return $this;
    }

    public function getUnEmplacement(): ?Emplacement
    {
        return $this->unEmplacement;
    }

    public function setUnEmplacement(?Emplacement $unEmplacement): self
    {
        $this->unEmplacement = $unEmplacement;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_At;
    }

    public function setCreatedAt(\DateTimeImmutable $created_At): self
    {
        $this->created_At = $created_At;

        return $this;
    }

    public function updateQuantity($quantite){
        $quantity = $this->getQuantite();
        $quantity = intval($quantity);
        $quantite = intval($quantite);
        if ($quantite > 0){
            $quantity += $quantite;
        } 
        if ($quantite < 0){
            $quantity += $quantite;
        }
        return $quantity;
    }
}
