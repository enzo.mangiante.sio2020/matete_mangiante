<?php

namespace App\Entity;

use App\Repository\VendeurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=VendeurRepository::class)
 * @UniqueEntity(
 *     fields={"mail", "nom"},
 *     message="Ce mail ou ce nom d'utilisateur existe déjà."
 * )
 */
class Vendeur implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numTel;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     * min = 6,
     * max = 50,
     * minMessage = "Veuillez rentrer un mot de passe d'au moins {{limit}} caractères",
     * maxMessage = "Veuillez rentrer un mot de passe de maximum {{limit}} caractères")
     */
    private $password;

    /**
     * @ORM\Column(type="json")
     */
    public $roles = [];

    /**
     * @ORM\OneToMany(targetEntity=Annonce::class, mappedBy="leVendeur")
     */
    public $lesAnnonces;

    /**
     * @ORM\OneToMany(targetEntity=Emplacement::class, mappedBy="leVendeur", orphanRemoval=true)
     */
    public $lesEmplacements;
    /**
     * @Assert\EqualTo(propertyPath="password",message = "La confirmation ne correspond pas")
     * @Assert\NotBlank
     */
    public $confirm_password;

    public function __construct()
    {
        $this->lesAnnonces = new ArrayCollection();
        $this->lesEmplacements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNumTel(): ?string
    {
        return $this->numTel;
    }

    public function setNumTel(string $numTel): self
    {
        $this->numTel = $numTel;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getLesAnnonces(): Collection
    {
        return $this->lesAnnonces;
    }

    public function addLesAnnonce(Annonce $lesAnnonce): self
    {
        if (!$this->lesAnnonces->contains($lesAnnonce)) {
            $this->lesAnnonces[] = $lesAnnonce;
            $lesAnnonce->setLeVendeur($this);
        }

        return $this;
    }

    public function removeLesAnnonce(Annonce $lesAnnonce): self
    {
        if ($this->lesAnnonces->removeElement($lesAnnonce)) {
            // set the owning side to null (unless already changed)
            if ($lesAnnonce->getLeVendeur() === $this) {
                $lesAnnonce->setLeVendeur(null);
            }
        }

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->mail;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->mail;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function isAdmin(): bool
    {
        return in_array('ROLE_ADMIN', $this->roles);
    }

    public function addRoles($role)
    {
        array_push($this->roles, $role);
    }

    /**
     * @return Collection|Emplacement[]
     */
    public function getLesEmplacements(): Collection
    {
        return $this->lesEmplacements;
    }

    public function addLesEmplacement(Emplacement $lesEmplacement): self
    {
        if (!$this->lesEmplacements->contains($lesEmplacement)) {
            $this->lesEmplacements[] = $lesEmplacement;
            $lesEmplacement->setLeVendeur($this);
        }

        return $this;
    }

    public function removeLesEmplacement(Emplacement $lesEmplacement): self
    {
        if ($this->lesEmplacements->removeElement($lesEmplacement)) {
            // set the owning side to null (unless already changed)
            if ($lesEmplacement->getLeVendeur() === $this) {
                $lesEmplacement->setLeVendeur(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
