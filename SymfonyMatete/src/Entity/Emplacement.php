<?php

namespace App\Entity;

use App\Repository\EmplacementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmplacementRepository::class)
 */
class Emplacement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomVille;

    /**
     * @ORM\OneToMany(targetEntity=Annonce::class, mappedBy="unEmplacement")
     */
    private $lesAnnonces;

    /**
     * @ORM\ManyToOne(targetEntity=Vendeur::class, inversedBy="lesEmplacements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $leVendeur;

    public function __construct()
    {
        $this->lesAnnonces = new ArrayCollection();
        $this->leVendeur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getNomVille(): ?string
    {
        return $this->nomVille;
    }

    public function setNomVille(?string $nomVille): self
    {
        $this->nomVille = $nomVille;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getLesAnnonces(): Collection
    {
        return $this->lesAnnonces;
    }

    public function addLesAnnonce(Annonce $lesAnnonce): self
    {
        if (!$this->lesAnnonces->contains($lesAnnonce)) {
            $this->lesAnnonces[] = $lesAnnonce;
            $lesAnnonce->setUnEmplacement($this);
        }

        return $this;
    }

    public function removeLesAnnonce(Annonce $lesAnnonce): self
    {
        if ($this->lesAnnonces->removeElement($lesAnnonce)) {
            // set the owning side to null (unless already changed)
            if ($lesAnnonce->getUnEmplacement() === $this) {
                $lesAnnonce->setUnEmplacement(null);
            }
        }

        return $this;
    }

    public function getLeVendeur(): ?Vendeur
    {
        return $this->leVendeur;
    }

    public function setLeVendeur(?Vendeur $leVendeur): self
    {
        $this->leVendeur = $leVendeur;

        return $this;
    }

    public function __toString()
    {
        return $this->adresse;
    }
}
