<?php

namespace App\DataFixtures;

use App\Entity\Vendeur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class VendeurFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i = 0; $i < 5; ++$i) {
            $vendeur = new vendeur();
            $vendeur->setMail('enzo' . $i . '.M.66300@gmail.com');
            $vendeur->setNom('Test' . $i);
            $vendeur->setNumTel('07621867382');
            $vendeur->setpassword('toto');
            $manager->persist($vendeur);
        }
        $manager->flush();
    }
}
