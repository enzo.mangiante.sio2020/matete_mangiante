<?php

namespace App\Tests;

use App\Repository\VendeurRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestVisitingWhileLoggedInTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(VendeurRepository::class);

        $testUser = $userRepository->findOneByMail('enzo1.M.66300@gmail.com');

        $client->loginUser($testUser);

        $client->request('GET', '/profil/');
        $this->assertResponseIsSuccessful();
    }
}
