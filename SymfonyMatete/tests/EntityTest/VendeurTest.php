<?php

namespace App\Test;

use App\Entity\Vendeur;
use PHPUnit\Framework\TestCase;

class VendeurTest extends TestCase
{
    public function testVendeur()
    {
        $vendeur = new Vendeur();
        $vendeur->setMail('enzo.M.66300@gmail.com');
        $vendeur->setNom('Test');
        $vendeur->setNumTel('07621867382');
        $vendeur->setpassword('toto');

        $this->assertTrue('enzo.M.66300@gmail.com' == $vendeur->getMail());
        $this->assertTrue('Test' == $vendeur->getNom());
        $this->assertTrue('07621867382' == $vendeur->getNumTel());
        $this->assertTrue('toto' == $vendeur->getpassword());
    }
}
