<?php

namespace App\Test;

use App\Entity\Annonce;
use App\Entity\Categorie;
use App\Entity\Emplacement;
use App\Entity\Vendeur;
use PHPUnit\Framework\TestCase;

class AnnonceTest extends TestCase
{
    public function testAnnonce()
    {
        $vendeur = new Vendeur();
        $vendeur->setMail('enzo.M.66300@gmail.com');
        $vendeur->setNom('Test');
        $vendeur->setNumTel('07621867382');
        $vendeur->setpassword('toto');

        $categorie = new Categorie();
        $categorie->setLibelle('Fruit');

        $emplacement = new Emplacement();
        $emplacement->setAdresse('19 Rue Pasteur');

        $annonce = new Annonce();
        $date = new \DateTimeImmutable();
        $annonce->setLeVendeur($vendeur);
        $annonce->setUnEmplacement($emplacement);
        $annonce->setLaCategorie($categorie);
        $annonce->setDescription('zfzafeazfe');
        $annonce->setNomAnnonce('Test');
        $annonce->setQuantite(10);
        $annonce->setImage('zafzafeazfe');
        $annonce->setCreatedAt($date);

        $this->assertTrue($annonce->getLeVendeur() == $vendeur);
        $this->assertTrue($annonce->getLaCategorie() == $categorie);
        $this->assertTrue($annonce->getUnEmplacement() == $emplacement);
        $this->assertTrue('Test' == $annonce->getNomAnnonce());
        $this->assertTrue('zfzafeazfe' == $annonce->getDescription());
        $this->assertTrue(10 == $annonce->getQuantite());
        $this->assertTrue('zafzafeazfe' == $annonce->getImage());
        $this->assertTrue($annonce->getCreatedAt() == $date);
    }

    public function testUpdateQuantite(){
        $annonce = new Annonce();
        $date = new \DateTimeImmutable();
        $annonce->setDescription('zfzafeazfe');
        $annonce->setNomAnnonce('Test');
        $annonce->setImage('zafzafeazfe');
        $annonce->setCreatedAt($date);
        $annonce->setQuantite(10);
        $this->assertEquals(20, $annonce->updateQuantity(10));
    }
}
