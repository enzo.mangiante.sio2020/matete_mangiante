![coverage](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage)
# Doc Technique

## Technologie utilisé :

> - PHP 8.0.12
> - Symfony 4.24.1
> - XAMPP
> - MYSQL

## Matet

### Présentation et fonctionnement

Matete est une application et un site web permettant de mettre en contact acheteur et commerçant. Les vendeurs créent leur profil et annonce sur le site Web qui sera affiché sur la carte de l'application android. Les clients eux vont sur le site Web pour imprimer leur panier et aller voir directement le vendeur.
Le tout fonctionne avec une API reliant les 2.

### Fonctionnalités : Site Web

> - Inscription/Connexion/Deconnexion pour les vendeurs.
> - Mode Admin.
> - Création/Suppression/Modification d'annonces reliée à l'application, avec le nom/numéro/mail/adresse du vendeur.
> - Panier et création d'un PDF pour les clients.
> - Liste d'annonces.
> - Page profil.
> - Page Vendeur.

#### Fonctionnalités : Application android

> - Carte avec des points GPS venant du site Web.

#### Site Web :

> - Panier :![](Image/panier.png)
> - PDF : ![](Image/pdf.png)
> - Profil : ![](Image/profil1.png) ![](Image/profil2.png)
> - Annonce : ![](Image/CréationAnnonce.png) ![](Image/annonces.png) ![](Image/annonce.png)
> - Profil Vendeur : ![](Image/vendeur.png)

### Application

> - Carte sur le site Web ![](Image/appli.png)
> - Informations des markers ![](Image/annonceAppli.png)

#### Schéma d'architecture applicative :

> ![](Image/ArchitectureMATETE.PNG)

#### Diagramme des cas des utilisations :

> ![](Image/useCaseMATETE.png)

## Les url et codes/pass d’accès à des versions en ligne de l’application :

> - https://gitlab.com/enzo.mangiante.sio2020/matete_mangiante
> - Compte Admin : Matheo@gmail.com / Matheo66300

## CI/CD :

> - Tests Fonctionnels : 
    - Connexion d'un user.
    - Redirection si connexion à une page admin.

> - Tests Unitaires : 
    - Entity Annonce/Vendeur.

> - CD : Deploiement continue sur le serveur du Lycée Jean Lurçat.