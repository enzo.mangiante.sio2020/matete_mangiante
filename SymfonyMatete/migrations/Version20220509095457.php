<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220509095457 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE annonce (id INT AUTO_INCREMENT NOT NULL, la_categorie_id INT NOT NULL, le_vendeur_id INT DEFAULT NULL, un_emplacement_id INT NOT NULL, description LONGTEXT NOT NULL, nom_annonce VARCHAR(255) NOT NULL, quantite INT NOT NULL, image VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_F65593E5281042B9 (la_categorie_id), INDEX IDX_F65593E5A68C79F5 (le_vendeur_id), INDEX IDX_F65593E58FC57C57 (un_emplacement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE emplacement (id INT AUTO_INCREMENT NOT NULL, le_vendeur_id INT NOT NULL, adresse VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, nom_ville VARCHAR(255) DEFAULT NULL, INDEX IDX_C0CF65F6A68C79F5 (le_vendeur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vendeur (id INT AUTO_INCREMENT NOT NULL, mail VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, num_tel VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', UNIQUE INDEX UNIQ_7AF499965126AC48 (mail), UNIQUE INDEX UNIQ_7AF499966C6E55B5 (nom), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E5281042B9 FOREIGN KEY (la_categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E5A68C79F5 FOREIGN KEY (le_vendeur_id) REFERENCES vendeur (id)');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E58FC57C57 FOREIGN KEY (un_emplacement_id) REFERENCES emplacement (id)');
        $this->addSql('ALTER TABLE emplacement ADD CONSTRAINT FK_C0CF65F6A68C79F5 FOREIGN KEY (le_vendeur_id) REFERENCES vendeur (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E5281042B9');
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E58FC57C57');
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E5A68C79F5');
        $this->addSql('ALTER TABLE emplacement DROP FOREIGN KEY FK_C0CF65F6A68C79F5');
        $this->addSql('DROP TABLE annonce');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE emplacement');
        $this->addSql('DROP TABLE vendeur');
    }
}
